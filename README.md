# Partial Renderer

## Auther

Zili HE (zili.he@mines-albi.fr)

## Introduction

The project Partial Renderer is a physical based monochromatic ray-tracing renderer based on high performance Monte-Carlo algorithm developed by the group [Edstar](https://gitlab.com/edstar) and [Méso Star](https://gitlab.com/users/meso-star).

From a 3D scene, Partial Renderer aims to render:

- An image of irradiance.
- An image of the partial derivative of irradiance with respect to the reflectance of a 3D object in the scene.

The output images are in [P2-PGM](http://netpbm.sourceforge.net/doc/pgm.html) format, obtained through option -x and -y.
Original rendering results are obtained by standard output.

## Language

- C

## How to build

It has only been tested in Debian-based system.

### Free-style build

Partial-renderer relies on the [CMake](https://cmake.org/) and the [RCMake](https://gitlab.com/vaplv/rcmake/) package to build.
It also depends on the [Star3D](https://gitlab.com/meso-star/star-3d/), [Star3DAW](https://gitlab.com/meso-star/star-3daw), [StarSF](https://gitlab.com/meso-star/star-sf), [RSys](https://gitlab.com/vaplv/rsys/), [StarSP](https://gitlab.com/meso-star/star-sp), as well as on the [OpenMP](https://www.openmp.org/) specification to parallelize its computations.

> **Note: You need to build the 0.8 version of [StarSP](https://gitlab.com/meso-star/star-sp) instead of its newest version**

First ensure that CMake and a C compiler are installed on your system. Then install the RCMake package as well as all the aforementioned prerequisites. Finally generate the project from the `cmake/CMakeLists.txt` file by appending to the `CMAKE_PREFIX_PATH` variable the install directories of its dependencies.

### Easy build

Partial-renderer can be built based on [Star-Engine](https://gitlab.com/meso-star/star-engine).

> **Note: The [star-sp](https://gitlab.com/meso-star/star-sp) embedded in the current version of [Star-Engine](https://gitlab.com/meso-star/star-engine) is not comptible with our project. You need to build the 0.8 version of [star-sp](https://gitlab.com/meso-star/star-sp) and then replace the incomptible version embedded in the [Star-Engine](https://gitlab.com/meso-star/star-engine).**

Then:

```
$ mkdir build && cd build && cmake ../cmake -DCMAKE_PREFIX_PATH=<STAR_ENGINE_DIR>
```

Finally:

```
$ make
```

## How to use

### Free-style use

Feel free as a bird.

### Easy usage

Firstly source the [Star-Engine](https://gitlab.com/meso-star/star-engine) library:

```
$ source <STAR_ENGINE_DIR>/etc/star-engine.profile 
```

Use `$ ./build/crazy -h`to check the options.

An example:

Make sure that ./output exists

```
$ mkdir output/ 
```
Render the bunny.obj

```
$ ./build/crazy -p 0,0,6 -t 0,0,-6 -u 0,1,0 -F 60 -d 800x600 -n 256 -r 0.1 -i ./etc/bunny.obj -x ./output/bunny.pgm -y ./output/bunny_sens.pgm -f
```

Render the xyzrgb\_dragon.obj

```
./build/crazy -p 0,0,-300 -t 0,0,300 -u 0,1,0 -F 60 -d 800x600 -n 256 -r 0.3 -i ./etc/xyzrgb_dragon.obj -x ./output/dragon.pgm -y ./output/dragon_sens.pgm -f
```

## Acknowledgements
Tianyuan Zhang contributes to this note (tianyuan.zhang@mines-albi.fr).
