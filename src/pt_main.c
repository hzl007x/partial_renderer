/* Copyright (C) 2019 |Meso|Star> (zili.he@mines-albi.fr vincent.forest@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "pt_args.h"
#include "pt.h"
#include <rsys/mem_allocator.h>
#include <stdio.h>

int
main(int argc, char** argv)
{
  struct pt_args args = PT_ARGS_DEFAULT;
  struct pt pt = PT_NULL;
  size_t memsz;
  res_T res = RES_OK; /* Temporary variable used to check return status */
  int err = 0; /* Error code. 0 means that no error occurs */

  res = pt_args_init(&args, argc, argv);
  if(res != RES_OK) goto error;
  if(args.quit) goto exit;
  res = pt_init(&args, &pt);
  if(res != RES_OK) goto error;
  res = pt_run(&pt);
  if(res != RES_OK) goto error;

exit:
  pt_args_release(&args);
  pt_release(&pt);
  memsz = mem_allocated_size();
  if(memsz != 0) {
    fprintf(stderr, "Overall memory leaks: %lu Bytes\n", (unsigned long)memsz);
    err = -1; /* Notify a memory leak error */
  }
  return err;
error:
  err = -1; /* An error occurs */
  goto exit;
}
