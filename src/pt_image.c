/* Copyright (C) 2019 |Meso|Star> (zili.he@mines-albi.fr vincent.forest@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "pt_image.h"
#include "pt.h"
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>
struct pt_image {
  struct pt_pixel* pixels;
  size_t width;
  size_t height;
  struct pt* pt;
  ref_T ref;
};
static void
image_release(ref_T* reference)
{
  struct pt_image* image;
  ASSERT(reference);
  image = CONTAINER_OF(reference, struct pt_image, ref);
  if(image->pixels) MEM_RM(&image->pt->allocator, image->pixels);
  MEM_RM(&image->pt->allocator, image);
}
res_T
pt_image_create
  (struct pt* pt,
  const size_t width,
  const size_t height,
  struct pt_image** image)
{
  struct pt_image* img = NULL;
  res_T res = RES_OK;

  if(!pt || !image) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: invalid NULL argument\n", FUNC_NAME);
    res = RES_BAD_ARG;
    goto error;
  }
  if(!width || !height) {
    logger_print(&pt->logger, LOG_ERROR, 
      "%s: invalid image définition `%lux%lu'\n",
      FUNC_NAME, (unsigned long)width, (unsigned long)height);
    res = RES_BAD_ARG;
    goto error;
  }
  img = MEM_CALLOC(&pt->allocator, 1, sizeof(*img));
  if(!img) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: could not allocate the image data structure\n", FUNC_NAME);
    res = RES_MEM_ERR;
    goto error;
  }
  img->pt = pt;
  ref_init(&img->ref);
  img->pixels = MEM_CALLOC(&pt->allocator, width*height, sizeof(*img->pixels));
  if(!img->pixels) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: could not allocate the image buffer\n", FUNC_NAME);
    res = RES_MEM_ERR;
    goto error;
  }
  img->width = width;
  img->height = height;

exit:
  if(image) *image = img;
  return res;
error:
  if(img != NULL) {
    pt_image_ref_put(img);
    img = NULL;
  }
  goto exit;
}
size_t
pt_image_get_width(const struct pt_image* image)
{
  ASSERT(image);
  return image->width;
}

size_t
pt_image_get_height(const struct pt_image* image)
{
  ASSERT(image);
  return image->height;
}
struct pt_pixel*
pt_image_at(struct pt_image* image, const size_t x,  const size_t y)
{
  ASSERT(image && x < image->width && y < image->height);
  return &image->pixels[y*image->width + x];
}
void
pt_image_ref_get(struct pt_image* image)
{
  ASSERT(image);
  ref_get(&image->ref);
}

void
pt_image_ref_put(struct pt_image* image)
{
  ASSERT(image);
  ref_put(&image->ref, image_release);
}
