/* Copyright (C) 2019 |Meso|Star> (zili.he@mines-albi.fr vincent.forest@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* Provide fdopen support */

#include "pt.h"
#include "pt_args.h"
#include "pt_camera.h"
#include "pt_image.h"
#include <errno.h>
#include <fcntl.h> /* Declare the open function */
#include <unistd.h> /* Declare the close function */
#include <sys/stat.h> /* Define the S_IRUSR & S_IWUSR constants */
#include <star/s3d.h>
#include <star/s3daw.h>
#include <star/ssf.h>
#include <omp.h>
#include <star/ssp.h>
static int
discard_self_hit
  (const struct s3d_hit* hit,
   const float ray_org[3],
   const float ray_dir[3],
   const float ray_range[3],
   void* ray_data,
   void* filter_data);

static void
draw_pixel
  (struct pt* pt,
   struct ssp_rng* rng,
   const size_t ipixel);

static double
compute_radiance
  (struct pt* pt,
   struct ssp_rng* rng,
   const double ray_org[3],
   const double ray_dir[3],
   struct s3d_primitive primitive_0,
   double *res);

static void
print_out(const char* msg, void* ctx)
{
  (void)ctx; /* Avoid the "unused variable" warning */
  fprintf(stderr, "\x1b[1m\x1b[32m>\x1b[0m %s", msg);
}

static void
print_err(const char* msg, void* ctx)
{
  (void)ctx; /* Avoid the "unused variable" warning */
  fprintf(stderr, "\x1b[31merror:\x1b[0m %s", msg);
}

static void
print_warn(const char* msg, void* ctx)
{
  (void)ctx; /* Avoid the "unused variable" warning */
  fprintf(stderr, "\x1b[33mwarning:\x1b[0m %s", msg);
}
static res_T
open_output_stream
  (struct pt* pt,
   const char* filename,
   const int force_overwriting,
   FILE** out_fp)
{
  FILE* fp = NULL;
  int fd = -1; /* File descriptor */
  res_T res = RES_OK;
  ASSERT(pt && filename && out_fp);

  if(force_overwriting) {
    fp = fopen(filename, "w");
    if(!fp) {
      logger_print(&pt->logger, LOG_ERROR,
        "%s: could not open the output file `%s'\n", FUNC_NAME, filename);
      res = RES_IO_ERR; /* Notify an Input/Output error */
      goto error;
    }
  } else {
    fd = open(filename, O_CREAT|O_EXCL|O_TRUNC|O_WRONLY, S_IRUSR|S_IWUSR);
    if(fd < 0) { /* An error occurs */
      if(errno == EEXIST) {
        logger_print(&pt->logger, LOG_ERROR,
          "%s: the output file `%s' already exists\n", FUNC_NAME, filename);
        res = RES_IO_ERR;
        goto error;
      } else {
        logger_print(&pt->logger, LOG_ERROR,
          "%s: unexpected error while opening the output file `%s'\n",
          FUNC_NAME, filename);
        res = RES_IO_ERR;
        goto error;
      }
    }

    fp = fdopen(fd, "w");
    if(!fp) {
      logger_print(&pt->logger, LOG_ERROR,
        "%s: could not open the output file `%s'\n", FUNC_NAME, filename);
      res = RES_IO_ERR;
      goto error;
    }
  }

exit:
  *out_fp = fp;
  return res;
error:
  if(fp) {
    fclose(fp);
    fp = NULL;
  } else {
    close(fd);
  }
  goto exit;
}
int
discard_self_hit
  (const struct s3d_hit* hit,
   const float ray_org[3],
   const float ray_dir[3],
   const float ray_range[3],
   void* ray_data,
   void* filter_data)
{
  const struct s3d_primitive* prim_from = ray_data;
  ASSERT(hit && ray_data && prim_from);

  /* Avoid "unused variable" warnings */
  (void)ray_org, (void)ray_range, (void)ray_dir, (void)filter_data;

  return S3D_PRIMITIVE_EQ(prim_from, &hit->prim);
}
static res_T
draw_image(struct pt* pt)
{
  size_t nthreads;
  int64_t ipixel, npixels;
  struct ssp_rng_proxy* rng_proxy = NULL;
  struct ssp_rng** rngs = NULL;
  size_t i;
  res_T res = RES_OK;
  ASSERT(pt);

  nthreads = (size_t)omp_get_num_procs();
  res = ssp_rng_proxy_create
    (&pt->allocator, SSP_RNG_MT19937_64, nthreads, &rng_proxy);
  if(res != RES_OK) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: could not create the proxy RNG\n", FUNC_NAME);
    goto error;
  }
  rngs = MEM_CALLOC(&pt->allocator, nthreads, sizeof(*rngs));
  if(!rngs) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: could not allocate the array of per thread RNG\n", FUNC_NAME);
    res = RES_MEM_ERR;
    goto error;
  }

  FOR_EACH(i, 0, nthreads) {
    res = ssp_rng_proxy_create_rng(rng_proxy, i, &rngs[i]);
    if(res != RES_OK) {
      logger_print(&pt->logger, LOG_ERROR,
        "%s: could not create the RNG of the thread %lu\n",
        FUNC_NAME, (unsigned long)i);
      goto error;
    }
  }

  npixels = (int64_t)
    (pt_image_get_width(pt->image) * pt_image_get_height(pt->image));
  omp_set_num_threads((int)nthreads);
  #pragma omp parallel for schedule(static, 1024/*chunk size*/)
  for(ipixel=0; ipixel < npixels; ++ipixel) {
    const int ithread = omp_get_thread_num();
    draw_pixel(pt, rngs[ithread], (size_t)ipixel);
  }

exit:
  if(rng_proxy) ssp_rng_proxy_ref_put(rng_proxy);
  if(rngs) {
    FOR_EACH(i, 0, nthreads) {
      if(rngs[i]) ssp_rng_ref_put(rngs[i]);
    }
    MEM_RM(&pt->allocator, rngs);
  }
  return res;
error:
  goto exit;
}

/*Monte-Carlo by pixel*/
void
draw_pixel(struct pt* pt, struct ssp_rng* rng, const size_t ipixel)
{
  size_t img_definition[2];
  size_t pix_idx[2];
  struct pt_pixel* pixel;
  double pix_size[2];
  double pix_low[2];
  size_t irealisation;
  ASSERT(pt && rng);

  /* Compute the image space pixel coordinates */
  img_definition[0] = pt_image_get_width(pt->image);
  img_definition[1] = pt_image_get_height(pt->image);
  pix_idx[0] = ipixel % img_definition[0];
  pix_idx[1] = ipixel / img_definition[0];
  /* Lower left corner of the pixel in the normalized image space */
  pix_size[0] = 1.0 / (double)img_definition[0];
  pix_size[1] = 1.0 / (double)img_definition[1];
  pix_low[0] = (double)pix_idx[0] * pix_size[0];
  pix_low[1] = (double)pix_idx[1] * pix_size[1];
  pixel = pt_image_at(pt->image, pix_idx[0], pix_idx[1]);
  *pixel = PT_PIXEL_NULL;

  FOR_EACH(irealisation, 0, pt->spp/*#samples per pixels*/) {
    double sample[2];
    double ray_org[3];
    double ray_dir[3];
    double w = 0; /* Monte-Carlo weight */
    double w_rho = 0; /* Monte-Carlo weight for reflectance sensitivity*/

    sample[0] = ssp_rng_canonical(rng)*pix_size[0] + pix_low[0];
    sample[1] = ssp_rng_canonical(rng)*pix_size[1] + pix_low[1];
    pt_camera_ray(pt->camera, sample, ray_org, ray_dir);
    w = compute_radiance(pt, rng, ray_org, ray_dir, S3D_PRIMITIVE_NULL,&w_rho);
    /*w_rho = compute_sensitivity(pt, rng, ray_org, ray_dir, S3D_PRIMITIVE_NULL);*/
    pixel->sum += w;
    pixel->sum2 += w*w;
    pixel->sum_rho += w_rho;
    pixel->sum2_rho += w_rho*w_rho;
    pixel->nweights += 1;
  }
}

double
compute_radiance
  (struct pt* pt,
   struct ssp_rng* rng,
   const double position[3],
   const double direction[3],
   struct s3d_primitive primitive_0,
   double *S)
{
  double cam_up[3], cam_up_abs[3];
  int skydome_axis;
  double pos[3], dir[3];
  double transmissivity = 1.0;
  double transmissivity_S = 0;
  float ray_range[2];
  float ray_org[3], ray_dir[3];
  /*struct s3d_primitive prim_from = S3D_PRIMITIVE_NULL;*/
  struct s3d_primitive prim_from = primitive_0;
  struct s3d_hit hit = S3D_HIT_NULL;
  double dir_next[3];
  double dir_temp[3];
  double N[3];
  double R;
  double n_refl = 0;
  int nbounces = 0;
  double L = 0; /* Radiance */
  ASSERT(pt && rng && position && direction); /* Check input variables */

  pt_camera_get_up(pt->camera, cam_up);
  cam_up_abs[0] = fabs(cam_up[0]);
  cam_up_abs[1] = fabs(cam_up[1]);
  cam_up_abs[2] = fabs(cam_up[2]);
  skydome_axis = cam_up_abs[0] > cam_up_abs[1]
    ? (cam_up_abs[0] > cam_up_abs[2] ? 0/*X axis*/ : 2/*Z axis*/)
    : (cam_up_abs[1] > cam_up_abs[2] ? 1/*Y axis*/ : 2/*Z axis*/);
  d3_set(pos, position);
  d3_set(dir, direction);
  for(;;) {
    ray_range[0] = 0;
    ray_range[1] = (float)INF;
    f3_set_d3(ray_org, pos);
    f3_set_d3(ray_dir, dir);
    f3_normalize(ray_dir, ray_dir);
    s3d_scene_view_trace_ray
      (pt->scnview, ray_org, ray_dir, ray_range, &prim_from, &hit);

    if(S3D_HIT_NONE(&hit)) {
      if(sign(dir[skydome_axis]) == sign(cam_up[skydome_axis])) {
        L = transmissivity;
        *S = transmissivity_S;
      } else {
        L = 0;
        *S = transmissivity_S;
      }
      break; /* Stop the random walk */
    }

    d3_set_f3(N, hit.normal);
    d3_normalize(N, N);
    if(d3_dot(N, dir) > 0) d3_minus(N, N);
    d3_minus(dir_temp, dir);
    R = ssf_bsdf_sample(pt->bsdf, rng, dir_temp, N, dir_next, NULL, NULL);
    if(transmissivity < 0.1) { /* Russian roulette on surface reflectivity */
      if(ssp_rng_canonical(rng) >= R) break;
    } else {
      transmissivity *= R;
      n_refl ++;
      transmissivity_S = n_refl*pow(R,n_refl-1);
      if(nbounces > 1000) { /* Russian roulette to stop long paths */
        if(ssp_rng_canonical(rng) > 0.5) {
          break;
        } else {
          transmissivity *= 2;
          transmissivity_S *= 2;
        }
      }
    }
    nbounces++;
    pos[0] = pos[0] + dir[0] * hit.distance;
    pos[1] = pos[1] + dir[1] * hit.distance;
    pos[2] = pos[2] + dir[2] * hit.distance;
    d3_set(dir, dir_next);
    prim_from = hit.prim;
  }

  return L;
}

static void
write_image_pgm(struct pt* pt)
{
  size_t img_size[2];
  size_t x, y;
  struct pt_pixel* pixel;
  double L;
  ASSERT(pt);

  img_size[0] = pt_image_get_width(pt->image);
  img_size[1] = pt_image_get_height(pt->image);
  fprintf(pt->output, "P2 %lu %lu\n",
    (unsigned long)img_size[0],
    (unsigned long)img_size[1]);
  fprintf(pt->output, "255\n"); /* Max value */
  FOR_EACH(y, 0, img_size[1]) {
    FOR_EACH(x, 0, img_size[0]) {
      pixel = pt_image_at(pt->image, x, y);
      ASSERT(pixel->nweights);
      L = pixel->sum / (double)pixel->nweights;
      if(L < 0.0031308) {
        L = L * 12.92;
      } else {
        L = 1.055 * pow(L, 1.0/2.4) - 0.055;
      }
      L = CLAMP(L, 0, 1);
      fprintf(pt->output, "%u\n", (unsigned char)(L * 255.0));
    }
  }
}

static void
write_ori_res(struct pt* pt)
{
  size_t img_size[2];
  size_t x, y;
  struct pt_pixel* pixel;
  double E,SE;
  ASSERT(pt);

  img_size[0] = pt_image_get_width(pt->image);
  img_size[1] = pt_image_get_height(pt->image);

  /*first line : resolution*/
  fprintf(pt->output_std,"irradiance-rendering-results\n");
  fprintf(pt->output_std, "resolution %lu %lu\n",
    (unsigned long)img_size[0],
    (unsigned long)img_size[1]);

  /*E and SE for each pixel*/
  FOR_EACH(y, 0, img_size[1]) {
    FOR_EACH(x, 0, img_size[0]) {
      pixel = pt_image_at(pt->image, x, y);
      ASSERT(pixel->nweights);
      E = pixel->sum / (double)pixel->nweights;
      SE = sqrt(pixel->sum2/(double)pixel->nweights - E*E)/sqrt((double)pixel->nweights);
      fprintf(pt->output_std, "%g %g\n", E,SE);
    }
  }
}

static void
write_image_rho_pgm(struct pt* pt)
{
  size_t img_size[2];
  size_t x, y;
  struct pt_pixel* pixel;
  double L;
  ASSERT(pt);

  img_size[0] = pt_image_get_width(pt->image);
  img_size[1] = pt_image_get_height(pt->image);
  fprintf(pt->output_rho, "P2 %lu %lu\n",
    (unsigned long)img_size[0],
    (unsigned long)img_size[1]);
  fprintf(pt->output_rho, "255\n"); /* Max value */
  FOR_EACH(y, 0, img_size[1]) {
    FOR_EACH(x, 0, img_size[0]) {
      pixel = pt_image_at(pt->image, x, y);
      ASSERT(pixel->nweights);
      L = pixel->sum_rho / (double)pixel->nweights;
      if(L < 0.0031308) {
        L = L * 12.92;
      } else {
        L = 1.055 * pow(L, 1.0/2.4) - 0.055;
      }
      L = CLAMP(L, 0, 1);
      fprintf(pt->output_rho, "%u\n", (unsigned char)(L * 255.0));
    }
  }
}

static void
write_ori_res_rho(struct pt* pt)
{
  size_t img_size[2];
  size_t x, y;
  struct pt_pixel* pixel;
  double E,SE;
  ASSERT(pt);

  img_size[0] = pt_image_get_width(pt->image);
  img_size[1] = pt_image_get_height(pt->image);

  /*first line : resolution*/
  fprintf(pt->output_std,"sensitivity-rendering-results\n");
  fprintf(pt->output_std, "resolution %lu %lu\n",
    (unsigned long)img_size[0],
    (unsigned long)img_size[1]);

  /*E and SE for each pixel*/
  FOR_EACH(y, 0, img_size[1]) {
    FOR_EACH(x, 0, img_size[0]) {
      pixel = pt_image_at(pt->image, x, y);
      ASSERT(pixel->nweights);
      E = pixel->sum_rho / (double)pixel->nweights;
      SE = sqrt(pixel->sum2_rho/(double)pixel->nweights - E*E)/sqrt((double)pixel->nweights);
      fprintf(pt->output_std, "%g %g\n", E,SE);
    }
  }
}

res_T
pt_init(const struct pt_args* args, struct pt* pt)
{
  double image_ratio;
  struct s3daw* s3daw = NULL;
  struct s3d_scene* scn = NULL;
  size_t nshapes, ishape;
  res_T res = RES_OK;

  *pt = PT_NULL; /* Default state of the pt variable */

  res = mem_init_regular_allocator(&pt->allocator);
  if(res != RES_OK) {
    fprintf(stderr, "%s: could not initialise the memory allocator\n", FUNC_NAME);
    goto error;
  }
  pt->allocator_is_init = 1;
  res = logger_init(&pt->allocator, &pt->logger);
  if(res != RES_OK) {
    fprintf(stderr, "%s: could not initialise the logger\n", FUNC_NAME);
    goto error;
  }
  logger_set_stream(&pt->logger, LOG_OUTPUT, print_out, NULL);
  logger_set_stream(&pt->logger, LOG_ERROR, print_err, NULL);
  logger_set_stream(&pt->logger, LOG_WARNING, print_warn, NULL);
  pt->logger_is_init = 1;
  image_ratio = (double)args->image.definition[0] / (double)args->image.definition[1];
  res = pt_camera_create(pt, args->camera.pos, args->camera.tgt,
    args->camera.up, image_ratio, MDEG2RAD(args->camera.field_of_view),
    &pt->camera);
  if(res != RES_OK) goto error;
  res = pt_image_create(pt, args->image.definition[0], args->image.definition[1],
    &pt->image);
  if(res != RES_OK) goto error;
  pt->spp = args->image.spp;

  /* initial stdout for pt->output_std */
  pt->output_std = stdout;

  /* initial ouput stream for intensity image*/
  if(!args->output_filename) {
    pt->output = NULL;
  } else {
    /*stream for intensity*/
    res = open_output_stream
      (pt, args->output_filename, args->force_overwriting, &pt->output);
    if(res != RES_OK) goto error;
  }

  /* initial ouput stream for sensitivity image*/
  if(!args->output_filename_rho) {
    pt->output_rho = NULL;
  } else {
    /*stream for sensitivity*/
    res = open_output_stream
      (pt, args->output_filename_rho, args->force_overwriting, &pt->output_rho);
    if(res != RES_OK) goto error;
  }

  res = s3d_device_create
    (&pt->logger, &pt->allocator, 0/*verbosity level*/, &pt->s3d);
  if(res != RES_OK) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: coult nod create the Star-3D device\n", FUNC_NAME);
    goto error;
  }
  res = s3daw_create(&pt->logger, &pt->allocator, NULL, NULL, pt->s3d,
    1/*verbosity level*/, &s3daw);
  if(res != RES_OK) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: could not create the Star-3DAW device\n", FUNC_NAME);
    goto error;
  }
  if(args->input_filename) {
    res = s3daw_load(s3daw, args->input_filename);
  } else {
    res = s3daw_load_stream(s3daw, stdin);
  }
  if(res != RES_OK) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: could not load the OBJ file `%s'\n",
       FUNC_NAME, args->input_filename ? args->input_filename : "stdin");
    goto error;
  }
  res = s3d_scene_create(pt->s3d, &scn);
  if(res != RES_OK) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: could not create the Star-3D scene\n", FUNC_NAME);
    goto error;
  }
  s3daw_get_shapes_count(s3daw, &nshapes);
  FOR_EACH(ishape, 0, nshapes) {
    struct s3d_shape* shape;
    s3daw_get_shape(s3daw, ishape, &shape);

    res = s3d_mesh_set_hit_filter_function(shape, discard_self_hit, NULL);
    if(res != RES_OK) {
      logger_print(&pt->logger, LOG_ERROR,
        "%s: could not setup the hit filter function\n", FUNC_NAME);
      goto error;
    }

    res = s3d_scene_attach_shape(scn, shape);
    if(res != RES_OK) {
      logger_print(&pt->logger, LOG_ERROR,
        "%s: could not attach the ground geometry to its Star-3D scene\n",
        FUNC_NAME);
      goto error;
    }
  }
  res = s3d_scene_view_create(scn, S3D_TRACE, &pt->scnview);
  if(res != RES_OK) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: could not create the view of the Star-3D scene\n", FUNC_NAME);
    goto error;
  }
  res = ssf_bsdf_create(&pt->allocator, &ssf_lambertian_reflection, &pt->bsdf);
  if(res != RES_OK) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: could not create the lambertian BSDF\n", FUNC_NAME);
      goto error;
  }
  res = ssf_lambertian_reflection_setup(pt->bsdf, args->reflectivity);
  if(res != RES_OK) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: could not setup the lambertian BSDF\n", FUNC_NAME);
    goto error;
  }

  if(s3daw) { s3daw_ref_put(s3daw); s3daw = NULL; }
  if(scn) { s3d_scene_ref_put(scn); scn = NULL; }

exit:
  return res;
error:
  if(s3daw) { s3daw_ref_put(s3daw); s3daw = NULL; }
  if(scn) { s3d_scene_ref_put(scn); scn = NULL; }
  pt_release(pt);
  goto exit;
}
void
pt_release(struct pt* pt)
{
  ASSERT(pt);

  if(pt->camera) pt_camera_ref_put(pt->camera);
  if(pt->image) pt_image_ref_put(pt->image);
  if(pt->s3d) s3d_device_ref_put(pt->s3d);
  if(pt->scnview) s3d_scene_view_ref_put(pt->scnview);
  if(pt->bsdf) ssf_bsdf_ref_put(pt->bsdf);
  if(pt->output && pt->output != stdout) fclose(pt->output);
  if(pt->output_rho) fclose(pt->output_rho);
  if(pt->logger_is_init) logger_release(&pt->logger);
  if(pt->allocator_is_init) {
    const size_t memsz = MEM_ALLOCATED_SIZE(&pt->allocator);
    if(memsz) { /* Check memory leaks */
      fprintf(stderr, "%s: memory leaks: %lu Bytes\n",
        FUNC_NAME, (unsigned long)memsz);
    }
    mem_shutdown_regular_allocator(&pt->allocator);
  }
  *pt = PT_NULL;
}

res_T
pt_run(struct pt* pt)
{
  res_T res = RES_OK;
  ASSERT(pt);

  res = draw_image(pt);
  if(res != RES_OK) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: error during the rendering\n", FUNC_NAME);
    goto error;
  }

  /*write ./pgm image*/
  if(pt->output != NULL) write_image_pgm(pt);
  if(pt->output_rho != NULL) write_image_rho_pgm(pt);

  /*standard ouput original rendering res*/
  write_ori_res(pt);
  write_ori_res_rho(pt);

exit:
  return res;
error:
  goto exit;
}
