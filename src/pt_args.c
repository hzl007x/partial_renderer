/* Copyright (C) 2019 |Meso|Star> (zili.he@mines-albi.fr vincent.forest@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 2 /* getopt support */

#include "pt_args.h"
#include <getopt.h>
#include <rsys/cstr.h>

static res_T
parse_definition(const char* string, unsigned definition[2])
{
  size_t nintegers;
  res_T res = RES_OK;
  ASSERT(string && definition);

  res = cstr_to_list_uint(string, 'x', definition, &nintegers, 2);
  if(res != RES_OK) /* Error during conversion */
    return res;
  if(nintegers != 2) /* Too many integers in the submitted string */
    return RES_BAD_ARG;
  if(definition[0] > 16384 /* Definition is too large */
  || definition[1] > 16384)
    return RES_BAD_ARG;

  return RES_OK;
}
static res_T
parse_real3(const char* string, double reals[3])
{
  size_t nreals;
  res_T res = RES_OK;
  ASSERT(string && reals);

  res = cstr_to_list_double(string, ',', reals, &nreals, 3);
  if(res == RES_OK && nreals != 3) res = RES_BAD_ARG;

  return res;
}
static void
print_help(const char* cmd_name)
{
  ASSERT(cmd_name);
  printf("Usage: %s [OPTION] ...\n", cmd_name);
  printf("Render a scene with a Monte-Carlo backward path-tracer (irradiance and its sensitivity on reflectivity).\n");
  printf("Original results can be obtained by standard-output (irradiance + sensitivity).\n\n");
  printf(
    "  -d WIDTHxHEIGHT  image definition. Default value is %ux%u.\n",
    PT_ARGS_DEFAULT.image.definition[0],
    PT_ARGS_DEFAULT.image.definition[1]);
  printf(
    "  -F               define the camera field of view (in degrees). Its default\n"
    "                   value is %g degrees. Its range: (0,pi).\n",
    PT_ARGS_DEFAULT.camera.field_of_view);
  printf(
    "  -f               overwrite the OUTPUT file if it already exists.\n");
  printf(
    "  -h               display this help and exit.\n");
  printf(
    "  -i INPUT         file of the scene to render. If not defined, scene data are\n"
    "                   read from standard input.\n");
  printf(
    "  -n NREALISATIONS number of per pixel realisations.\n");
  printf(
    "  -x OUTPUT        file where the rendered intensity (L) image is written.\n");
  printf(
    "  -y OUTPUT        file where the rendered reflectivity sensitivity (dL/drho) image is written.\n");
  printf(
    "  -p X,Y,Z         camera position. Its default value is [%g, %g, %g].\n",
    PT_ARGS_DEFAULT.camera.pos[0],
    PT_ARGS_DEFAULT.camera.pos[1],
    PT_ARGS_DEFAULT.camera.pos[2]);
  printf(
    "  -r REFLECTIVITY  scene reflectivity. Its default value is %g. Its range: [0,1]\n",
    PT_ARGS_DEFAULT.reflectivity);
  printf(
    "  -t X,Y,Z         camera target. Its default value is [%g, %g, %g].\n",
    PT_ARGS_DEFAULT.camera.tgt[0],
    PT_ARGS_DEFAULT.camera.tgt[1],
    PT_ARGS_DEFAULT.camera.tgt[2]);
  printf(
    "  -u X,Y,Z         camera up vector. Its default value is [%g, %g, %g].\n",
    PT_ARGS_DEFAULT.camera.up[0],
    PT_ARGS_DEFAULT.camera.up[1],
    PT_ARGS_DEFAULT.camera.up[2]);
  printf("\n");
  printf(
    "%s This is free software released under the GNU GPL\n"
    "license, version 3 or later. You are free to change or redistribute it under\n"
    "certain conditions <http://gnu.org/licenses/gpl.html>.\n",
    cmd_name);
}
res_T
pt_args_init(struct pt_args* args, int argc, char** argv)
{
  int opt;
  res_T res = RES_OK;
  ASSERT(args && argc && argv); /* Pre-condition */

  *args = PT_ARGS_DEFAULT; /* Setup default argument values */

  while((opt = getopt(argc, argv, "d:i:n:p:t:u:x:y:F:fr:h")) != -1) {
    switch(opt) {
      case 'n':
        res = cstr_to_uint(optarg, &args->image.spp);
        if(res == RES_OK && args->image.spp == 0) res = RES_BAD_ARG;
        break;
      case 'd':
        res = parse_definition(optarg, args->image.definition);
        break;
      case 'p': res = parse_real3(optarg, args->camera.pos); break;
      case 't': res = parse_real3(optarg, args->camera.tgt); break;
      case 'u': res = parse_real3(optarg, args->camera.up); break;
      case 'F': res = cstr_to_double(optarg, &args->camera.field_of_view); break;
      case 'r':
        res = cstr_to_double(optarg, &args->reflectivity);
        if(res == RES_OK && (args->reflectivity>1 || args->reflectivity <0)) {
          res = RES_BAD_ARG;
        }
        break;
      case 'i': args->input_filename = optarg; break;
      case 'x': args->output_filename = optarg; break;
      case 'y': args->output_filename_rho = optarg; break;
      case 'f': args->force_overwriting = 1; break;
      case 'z': args->output_ori_res = 1; break;
      case 'h':
        print_help(argv[0]);
        args->quit = 1;
        goto exit;
      default: res = RES_BAD_ARG; break; /* Unexpected option */
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option argument '%s' -- '%c'\n",
          argv[0], optarg, opt);
      }
      goto error;
    }
  }

exit:
  return res;
error:
  pt_args_release(args);
  goto exit;
}
void
pt_args_release(struct pt_args* args)
{
  ASSERT(args);
  (void)args; /* Avoid the "unused variable" warning */
}
