/* Copyright (C) 2019 |Meso|Star> (zili.he@mines-albi.fr vincent.forest@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef PT_CAMERA_H
#define PT_CAMERA_H

#include <rsys/rsys.h>
struct pt_camera; /* Forward declaration of the camera data type */
struct pt;
extern LOCAL_SYM res_T
pt_camera_create
  (struct pt* pt,
   const double position[3],
   const double target[3],
   const double up[3],
   const double image_ratio, /* Width / Height */
   const double field_of_view, /* Vertical field of view in radians */
   struct pt_camera** camera); /* Output camera */
extern LOCAL_SYM void
pt_camera_ref_get
  (struct pt_camera* camera);

extern LOCAL_SYM void
pt_camera_ref_put
  (struct pt_camera* camera);
extern LOCAL_SYM void
pt_camera_get_up
  (const struct pt_camera* camera,
   double up[3]);
extern LOCAL_SYM void
pt_camera_ray
  (const struct pt_camera* camera,
   const double sample[2],
   double ray_org[3],
   double ray_dir[3]);

#endif /* PT_CAMERA_H */
