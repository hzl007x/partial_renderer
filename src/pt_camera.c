/* Copyright (C) 2019 |Meso|Star> (zili.he@mines-albi.fr vincent.forest@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "pt_camera.h"
#include "pt.h"
#include <rsys/math.h> /* Define the PI constant */
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>
#include <rsys/double3.h>
#include <math.h> /* tan definition */
struct pt_camera {
  double axis_x[3];
  double axis_y[3];
  double axis_z[3];
  double position[3]; /* frame origin */
  double up[3];
  struct pt* pt;
  ref_T ref;
};
static void
camera_release(ref_T* reference)
{
  struct pt_camera* camera;
  ASSERT(reference);

  camera = CONTAINER_OF(reference, struct pt_camera, ref);
  MEM_RM(&camera->pt->allocator, camera);
}
res_T
pt_camera_create
  (struct pt* pt,
   const double position[3],
   const double target[3],
   const double up[3],
   const double image_ratio,
   const double field_of_view,
   struct pt_camera** camera)
{
  struct pt_camera* cam = NULL;
  double h;
  res_T res = RES_OK;

  if(!pt || !position || !target || !up || !camera) {
    logger_print(&pt->logger, LOG_ERROR, 
      "%s: invalid NULL argument\n", FUNC_NAME);
    res = RES_BAD_ARG;
    goto error;
  }
  if(field_of_view <= 0 || field_of_view >= PI) {
    logger_print(&pt->logger, LOG_ERROR,
      "%s: invalid vertical field of view `%g', its range: (0,pi)\n", FUNC_NAME, field_of_view);
    res = RES_BAD_ARG;
    goto error;
  }
  if(image_ratio <= 0) {
    logger_print(&pt->logger, LOG_ERROR, 
      "%s: invalid image ratio `%g'\n", FUNC_NAME, field_of_view);
    res = RES_BAD_ARG;
    goto error;
  }
  cam = MEM_CALLOC(&pt->allocator, 1, sizeof(*cam));
  if(!cam) {
    logger_print(&pt->logger, LOG_ERROR, 
      "%s: could not allocate the camera data structure\n", FUNC_NAME);
    res = RES_MEM_ERR;
    goto error;
  }
  cam->pt = pt;
  ref_init(&cam->ref);
  d3_set(cam->position, position);
  d3_sub(cam->axis_z, target, position);
  d3_cross(cam->axis_x, cam->axis_z, up);
  d3_cross(cam->axis_y, cam->axis_z, cam->axis_x);

  if(d3_normalize(cam->axis_x, cam->axis_x) <= 0
  || d3_normalize(cam->axis_y, cam->axis_y) <= 0
  || d3_normalize(cam->axis_z, cam->axis_z) <= 0) {
    logger_print(&pt->logger, LOG_ERROR, 
      "%s: invalid camera point of view:\n"
      "\tposition = [%g, %g, %g]\n"
      "\ttarget   = [%g, %g, %g]\n"
      "\tup       = [%g, %g, %g]\n",
      FUNC_NAME,
      position[0], position[1], position[2],
      target[0], target[1], target[2],
      up[0], up[1], up[2]);
    res = RES_BAD_ARG;
    goto error;
  }
  d3_set(cam->up, up);
  h = 1.0/* Half length of the pyramid base along Y */
    / tan(field_of_view*0.5);
  d3_muld(cam->axis_z, cam->axis_z, h);
  d3_muld(cam->axis_x, cam->axis_x, image_ratio);

exit:
  if(camera) *camera = cam;
  return res;
error:
  if(cam != NULL) {
    pt_camera_ref_put(cam);
    cam = NULL;
  }
  goto exit;
}
void
pt_camera_get_up(const struct pt_camera* camera, double up[3])
{
  ASSERT(camera && up);
  d3_set(up, camera->up);
}
void
pt_camera_ray
  (const struct pt_camera* camera,
   const double sample[2],
   double ray_org[3],
   double ray_dir[3])
{
  double sample_adjusted[2];
  double x[3], y[3];
  ASSERT(camera && sample && ray_org && ray_dir);
  ASSERT(sample[0] >= 0 && sample[0] < 1);
  ASSERT(sample[1] >= 0 && sample[1] < 1);
  sample_adjusted[0] = sample[0] * 2 - 1;
  sample_adjusted[1] = sample[1] * 2 - 1;
  d3_muld(x, camera->axis_x, sample_adjusted[0]);
  d3_muld(y, camera->axis_y, sample_adjusted[1]);
  d3_add(ray_dir, x, y);
  d3_add(ray_dir, ray_dir, camera->axis_z);
  d3_normalize(ray_dir, ray_dir);
  d3_set(ray_org, camera->position);
}
void
pt_camera_ref_get(struct pt_camera* camera)
{
  ASSERT(camera);
  ref_get(&camera->ref);
}
void
pt_camera_ref_put(struct pt_camera* camera)
{
  ASSERT(camera);
  ref_put(&camera->ref, camera_release);
}
