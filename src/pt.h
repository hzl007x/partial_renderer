/* Copyright (C) 2019 |Meso|Star> (zili.he@mines-albi.fr vincent.forest@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef PT_H
#define PT_H

#include <rsys/rsys.h>
#include <rsys/mem_allocator.h>
#include <rsys/logger.h>
#include <stdio.h>
struct pt_args;
struct pt_camera;
struct pt_image;
struct s3d_device;
struct s3d_scene_view;
struct ssf_bsdf;
struct pt {
  struct mem_allocator allocator;
  struct logger logger;
  struct pt_camera* camera;
  struct pt_image* image;
  size_t spp; /* # Samples per pixel */
  FILE* output; /* .pmg for intensity */
  FILE* output_rho; /* .pmg for sensitivity */
  FILE* output_std;
  struct s3d_device* s3d;
  struct s3d_scene_view* scnview;
  struct ssf_bsdf* bsdf;
  int allocator_is_init;
  int logger_is_init;
};
static const struct pt PT_NULL; /* Implicitly initialised to 0 */
extern LOCAL_SYM res_T
pt_init
  (const struct pt_args* args,
   struct pt* pt);
extern LOCAL_SYM void
pt_release
  (struct pt* pt);
extern LOCAL_SYM res_T
pt_run
  (struct pt* pt);

#endif /* PT_H */
