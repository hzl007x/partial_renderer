/* Copyright (C) 2019 |Meso|Star> (zili.he@mines-albi.fr vincent.forest@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef PT_IMAGE_H
#define PT_IMAGE_H

#include <rsys/rsys.h> /* Define the LOCAL_SYM macro and the res_T type */
struct pt_image;
struct pt; /* Forward declaration */
struct pt_pixel {
  double sum; /* Sum of Monte-Carlo weights */
  double sum2; /* Sum of squared Monte-Carlo weights */
  double sum_rho; /* Sum of Monte-Carlo weights for reflectance sensitivity*/
  double sum2_rho; /* Sum of squared Monte-Carlo weights for reflectance sensitivity*/
  size_t nweights; /* Numober of accumulated weights */
};

static const struct pt_pixel PT_PIXEL_NULL = {0, 0, 0, 0, 0};

extern LOCAL_SYM res_T
pt_image_create
  (struct pt* pt,
   const size_t width,
   const size_t height,
   struct pt_image** image);

extern LOCAL_SYM void
pt_image_ref_get
  (struct pt_image* image);

extern LOCAL_SYM void
pt_image_ref_put
  (struct pt_image* image);
extern LOCAL_SYM size_t
pt_image_get_width
  (const struct pt_image* image);

extern LOCAL_SYM size_t
pt_image_get_height
  (const struct pt_image* image);
extern LOCAL_SYM struct pt_pixel*
pt_image_at
  (struct pt_image* image,
   const size_t x,
   const size_t y);

#endif /* PT_IMAGE_H */
