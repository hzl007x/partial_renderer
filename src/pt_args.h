/* Copyright (C) 2019 |Meso|Star> (zili.he@mines-albi.fr vincent.forest@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef PT_ARGS_H
#define PT_ARGS_H

#include <rsys/rsys.h>
struct pt_args {
  struct {
    unsigned definition[2];
    unsigned spp; /* #samples per pixel */
  } image;
  struct {
    double pos[3]; /* Position */
    double tgt[3]; /* Targeted point */
    double up[3]; /* Vertical axis */
    double field_of_view; /* Field of view in degrees */
  } camera;
  double reflectivity;
  const char* input_filename;
  const char* output_filename;
  const char* output_filename_rho;
  int force_overwriting;
  int quit;
  int output_ori_res;
};
static const struct pt_args PT_ARGS_DEFAULT = {
  {
    {320, 240},/* Image definition */
    1 /* #samples per pixel */
  },
  {
    {0, 0, 0}, /* Camera position */
    {0, 1, 0}, /* Camera target */
    {0, 0, 1}, /* Camera up vector */
    70.0 /* Camera field of view in degrees */
  },
  1, /* Reflectivity */
  NULL, /* Input filename */
  NULL, /* Output filename */
  NULL, /* Output_rho filename */
  0, /* Force overwriting */
  0, /* Quit */
  0 /* dont output ori res */
};
extern LOCAL_SYM res_T
pt_args_init
  (struct pt_args* args,
   int argc, /* Number of arguments */
   char** argv); /* List of arguments */
extern LOCAL_SYM void
pt_args_release
  (struct pt_args* args);

#endif /* PT_ARGS_H */
